package com.pym.lovematchingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남성"),
    WOMEN("여성");

    private final String genderType;
}
