package com.pym.lovematchingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveMatchingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoveMatchingApiApplication.class, args);
	}

}
