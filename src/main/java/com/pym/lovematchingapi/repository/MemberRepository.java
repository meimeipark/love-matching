package com.pym.lovematchingapi.repository;

import com.pym.lovematchingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository <Member, Long> {
}
