package com.pym.lovematchingapi.repository;

import com.pym.lovematchingapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
