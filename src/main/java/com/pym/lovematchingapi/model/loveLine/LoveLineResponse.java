package com.pym.lovematchingapi.model.loveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineResponse {
    private Long id;
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String memberGender;
    private String lovePhoneNumber;
}
