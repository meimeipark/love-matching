package com.pym.lovematchingapi.service;

import com.pym.lovematchingapi.entity.LoveLine;
import com.pym.lovematchingapi.entity.Member;
import com.pym.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.pym.lovematchingapi.model.loveLine.LoveLineItem;
import com.pym.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.pym.lovematchingapi.model.loveLine.LoveLineResponse;
import com.pym.lovematchingapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request){
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLiveLines(){
        List<LoveLine> originList = loveLineRepository.findAll();

        List<LoveLineItem> result = new LinkedList<>();

        for (LoveLine loveLine: originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public LoveLineResponse getLoveLine(long id){
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();

        LoveLineResponse response = new LoveLineResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setMemberGender(originData.getMember().getGender().getGenderType());
        response.setLovePhoneNumber(originData.getLovePhoneNumber());

        return response;
    }

    public void putPhoneNumber(long id, LoveLinePhoneNumberChangeRequest request){
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();

        originData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(originData);

    }
}
