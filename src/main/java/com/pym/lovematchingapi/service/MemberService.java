package com.pym.lovematchingapi.service;

import com.pym.lovematchingapi.entity.Member;
import com.pym.lovematchingapi.model.member.MemberCreateRequest;
import com.pym.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }
    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setGender(request.getGender());

        memberRepository.save(addData);
    }
}
